#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>

#include "list.cpp"

#ifndef __shell
#define __shell

using namespace std;

bool repassing = false;

void shellSort(list arr[], int n, int &bigO)
{	
   ofstream shell50;
   ofstream shell5000;
   ofstream shell100000;
 
   int x = 0;
   bool swapped = false;
   list cpy[n];
   copy(arr, arr+n, cpy);
   while (!swapped){

      //initially space is n/2 , then it is gradually reduced with each ieration
      for (int space= n/2; space>0; space/=2)
      {
         for (int i=space; i<n; i++)
         {
            //check for all elements at positions multipe to space
            for (int j=i-space;j >= 0 && cpy[j].key > cpy[j+space].key;j-=space){
            std::swap( cpy[j].data, cpy[j+space].data);
	    bigO++;
            }
         }
      }
      swapped = true;
   }
	
     if(n==50)
      {
        shell50.open ("shell50.txt");

        shell50 << "list 50:" << endl << endl;
        for(x=0;x<50;x++)
        {
           shell50<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Shell Sort Big-O calculated value: O(n(log(n))^2) = 144.32495" << endl << "Shell Sort operations: " << bigO << endl;

        shell50.close();
      }

      if(n==5000 && repassing == false)
      {
        shell5000.open ("shell5000.txt");

        shell5000 << "list 5000:" << endl << endl;

        for(x=0;x<5000;x++)
        {
           shell5000<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Shell Sort Big-O calculated value: O(n(log(n))^2) = 68411.89546" << endl << "Shell Sort operations: " << bigO << endl;

        shell5000.close();
      }
      if(n==100000)
      {
        shell100000.open ("shell100000.txt");

        shell100000 << "list 100000:" << endl << endl;

        for(x=0;x<100000;x++)
        {
           shell100000<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Shell Sort Big-O calculated value: O(n(log(n))^2) = 2500000" << endl << "Shell Sort operations: " << bigO << endl;

        shell100000.close();
      }

}
#endif 
