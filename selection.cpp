/*************************************************************************************
Selection Sort Pseudo Code

Description of the Problem:
	This program will generate three unsorted lists of data of different sizes: 50, 5000, and 100,000 items.
	The unsorted lists will consist of a data structure that contains the key - a random integer and a random string
Pseudo Code:
  Selection sort
Data:
  bigO                                - iterations counter
  key	                              - randomly generated key used for sort
  data	                              - randomly generated data used in sort

Parameter:
  arr[]                               - copied arry from main
  n                                   - number of arry
  bigO                                - iterator counter
 ************************************************************************************/
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include "list.cpp"

#ifndef __select
#define __select

using namespace std;

void selectionSort(list arr[], int n,int &bigO)
{
     ofstream sels50;
     ofstream sels5000;
     ofstream sels100000;

     int i, j, minIndex;
     int x = 0;
	  list tmp;
	  list cpy[n];
	  copy(arr, arr+n, cpy);
      for (i = 0; i < n - 1; i++)
	  {
            minIndex = i;
            for (j = i + 1; j < n; j++)
			{
				bigO++;
				if (cpy[j].key < cpy[minIndex].key)
                        minIndex = j;
			}
            if (minIndex != i)
            {
                  tmp = cpy[i];
                  cpy[i].key = cpy[minIndex].key;
				  cpy[i].data = cpy[minIndex].data;

                  cpy[minIndex].key = tmp.key;
				  cpy[minIndex].data = tmp.data;
            }

      }

      if(n==50)
      {
        sels50.open ("sels50.txt");

        sels50 << "list 50:" << endl << endl;
        for(x=0;x<50;x++)
        {
           sels50<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Selection Sort Big-O calculated value: O(n^2) = 2500" << endl << "Selection sort operations: " << bigO << endl;

        sels50.close();
      }

      if(n==5000 && repassing == false)
      {
        sels5000.open ("sels5000.txt");

        sels5000 << "list 5000:" << endl << endl;

        for(x=0;x<5000;x++)
        {
           sels5000<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Selection Sort Big-O calculated value: O(n^2) = 2500" << endl << "Selection sort operations: " << bigO << endl;

        sels5000.close();
      }
      if(n==100000)
      {
        sels100000.open ("sels100000.txt");

        sels100000 << "list 100000:" << endl << endl;

        for(x=0;x<100000;x++)
        {
           sels100000<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Selection Sort Big-O calculated value: O(n^2) = 2500" << endl << "Selection sort operations: " << bigO << endl;

        sels100000.close();
      }

}


#endif
