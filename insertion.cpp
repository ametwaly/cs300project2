/*************************************************************************************
Insertion Pseudo Code

Description of the Problem:
	This program will generate three unsorted lists of data of different sizes: 50, 5000, and 100,000 items.
	The unsorted lists will consist of a data structure that contains the key - a random integer and a random string (data)

Data:

	int bigO = 0;												- iteration counter for each sort
	key												        - randomly generated key used for sort
	data												        - randomly generated data used in sort
	small												        - array of list 50
	medium													- array of list 5000
	large 												        - array of list 100,000

Functions:
 	insertionSort												- insertion sort called

 ************************************************************************************/
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include "list.cpp"

#ifndef __inser
#define __inser

using namespace std;


void insertionSort(list arr[], int length, int &bigO)
{
     ofstream sins50;
     ofstream sins5000;
     ofstream sins100000;

     int i, j;
     int x = 0;
	  list tmp;
	  list cpy[length];
	  copy(arr, arr+length, cpy);
      for (i = 1; i < length; i++)
	  {
            j = i;

            while (j > 0 && cpy[j - 1].key > cpy[j].key)
			{
                  tmp = cpy[j];
                  cpy[j].key = cpy[j - 1].key;
				  cpy[j].data = cpy[j - 1].data;

                  cpy[j - 1].key = tmp.key;
				  cpy[j - 1].data = tmp.data;
                  j--;
				 bigO++;
            }
      }

      if(length==50)
      {
        sins50.open ("sins50.txt");

        sins50 << "list 50:" << endl << endl;
        for(x=0;x<50;x++)
        {
           sins50<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Insertion Sort Big-O calculated value: O(n^2) = 2500" << endl << "Insertion Sort operations : " << bigO << endl;

        sins50.close();
      }

      if(length==5000 && repassing == false)
      {
        sins5000.open ("sins5000.txt");

        sins5000 << "list 5000:" << endl << endl;

        for(x=0;x<5000;x++)
        {
           sins5000<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Insertion Sort Big-O calculated value: O(n^2) = 2500" << endl << "Insertion Sort operations : " << bigO << endl;

        sins5000.close();
      }
      if(length==100000)
      {
        sins100000.open ("sins100000.txt");

        sins100000 << "list 100000:" << endl << endl;

        for(x=0;x<100000;x++)
        {
           sins100000<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Insertion Sort Big-O calculated value: O(n^2) = 2500" << endl << "Insertion Sort operations : " << bigO << endl;

        sins100000.close();
      }

}

#endif
