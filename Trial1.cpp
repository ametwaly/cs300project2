#include <iostream>
#include <cstdlib>
#include <cstring>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>

using namespace std;

struct list
{
	int key;
	double data;
};

void shellSort(list arr[], int n, int &bigO)
{
   //initially space is n/2 , then it is gradually reduced with each ieration
   for (int space= n/2; space>0; space/=2)
   {
      bigO++;
      for (int i=space; i<n; i+=1)
      {   
         int temp = arr[i];
         int j;   

         //check for all elements at positions multipe to space
         for (j=i;j>=space&&arr[j-space]>temp;j-=space)
         {

         bigO++;//increemnt the operations count

         arr[j]=arr[j-space];
         }

         arr[j]=temp;
         //increment operation
         bigO++;
      }
   }
}


void bubbleSort(list arr[], int n, int &bigO) 
{
      bool swapped = true;
      int j = 0;
	  
      list tmp;
      list cpy[n];
	  copy(arr, arr+n, cpy);
	  while (swapped) 
	  {
            swapped = false;
            j++;
            for (int i = 0; i < n - j; i++) 
			{
                  if (cpy[i].key > cpy[i + 1].key) 
				  {
                        tmp = cpy[i];
                        cpy[i].key = cpy[i + 1].key;
						cpy[i].data=cpy[i+1].data;
						
                        cpy[i + 1].key = tmp.key;
						cpy[i+1].data= tmp.data;
						
                        swapped = true;						
                  }
			 bigO++; 
            }
			
      }
}

void quickSort(list arr[], int left, int right, int &bigO) 
{
      int i = left, j = right;
	  
      list tmp;
      int pivot = arr[(left + right) / 2].key;
 
  
      while (i <= j) 
	  {
            while (arr[i].key < pivot)
			{
                  i++;
				  bigO++;
			}
            while (arr[j].key > pivot)
			{
                  j--;
				  bigO++;
			}
            if (i <= j) 
			{
                  tmp = arr[i];
                  arr[i].key = arr[j].key;
				  arr[i].data = arr[j].data;
				  
                  arr[j].key = tmp.key;
				  arr[j].data =tmp.data;
				  
                  i++;
                  j--;
				  
            }
		
      };
 
    
      if (left < j)
            quickSort(arr, left, j,bigO);
      if (i < right)
            quickSort(arr, i, right,bigO);
}

void selectionSort(list arr[], int n,int &bigO) 
{
      int i, j, minIndex; 
	  list tmp;
	  list cpy[n];
	  copy(arr, arr+n, cpy);	
      for (i = 0; i < n - 1; i++) 
	  {
            minIndex = i;
            for (j = i + 1; j < n; j++)
			{	
				bigO++;	
				if (cpy[j].key < cpy[minIndex].key)
                        minIndex = j;
			}
            if (minIndex != i) 
			{
                  tmp = cpy[i];
                  cpy[i].key = cpy[minIndex].key;
				  cpy[i].data = cpy[minIndex].data;
				  
                  cpy[minIndex].key = tmp.key;
				  cpy[minIndex].data = tmp.data;
            }
		
      }
}



void insertionSort(list arr[], int length, int &bigO) 
{
      int i, j;
	  list tmp;
	  list cpy[length];
	  copy(arr, arr+length, cpy);
      for (i = 1; i < length; i++) 
	  {
            j = i;
			 
            while (j > 0 && cpy[j - 1].key > cpy[j].key) 
			{
                  tmp = cpy[j];
                  cpy[j].key = cpy[j - 1].key;
				  cpy[j].data = cpy[j - 1].data;
				  
                  cpy[j - 1].key = tmp.key;
				  cpy[j - 1].data = tmp.data;
                  j--;
				 bigO++;
            }
      }
}

class BinarySearchTree
{
    private:
        struct tree_node
        {
           tree_node* left;
           tree_node* right;
           int key;
		   double data;
        };
        tree_node* root;
    public:
        BinarySearchTree()
        {
           root = NULL;
        }
        bool isEmpty() const 
        { return root==NULL; }
        void print_inorder();
        void inorder(tree_node*);
        void insert( int,double);

        
        
        
};

// Smaller elements go left
// larger elements go right

void BinarySearchTree::insert(int k, double d)
{
    tree_node* t = new tree_node;
    tree_node* parent;
    t->key = k;
	t->data = d;
    t->left = NULL;
    t->right = NULL;
    parent = NULL;
  //if empty node the data will be set as the root
  if(isEmpty()) 
    root = t;
  else
  {
    //Note: ALL insertions are as leaf nodes
    tree_node* curr;
    curr = root;
    // Find the Node's parent
    while(curr)
    {
        parent = curr;
        if(t->key > curr->key) 
			curr = curr->right;
        else 
			curr = curr->left;
		
    }
    if(t->key < parent->key)
       parent->left = t;
    else
       parent->right = t;
  }
}

void BinarySearchTree::print_inorder()
{    
      
       inorder(root);
      
       
}

void BinarySearchTree::inorder(tree_node* p)
{    
    
    if(p != NULL)
    {
       
        if(p->left) inorder(p->left);
         cout <<" "<<p->key <<endl;
		 //<<" "<< p->data <<endl; 
        if(p->right) inorder(p->right);
      
    }
    
    else return;
   
}

int main ()
{
        std::ofstream bub;
	std::ofstream shell;
	std::ofstream quick;
	std::ofstream insertion;
	std::ofstream selection;
	
	BinarySearchTree bsmall;
	BinarySearchTree bmed;
	BinarySearchTree blarge;
	
	srand((int)time(NULL));
	
	int i = 0;
	int bigO = 0;
	int n = 0;
	int d = 0;
	
	list small[100];
	list medium[500];
	list large[1000];
	list cpy[n];
	
	
	for (i=0;i<=99;i++)
	{
		small[i].key = (1 + rand());
		small[i].data = (double)rand(); 	
		
	}
	
	for (i=0;i<=499;i++)
	{
		medium[i].key = (1 + rand()); 
		medium[i].data = (double)rand();
	}
	
	for (i=0;i<=999;i++)
	{
		large[i].key = (1 + rand()); 
		large[i].data = (double)rand();
	}

	
/*************small sorts**************/
	cout << "Small Sorts" << endl;
	bubbleSort(small,100,bigO);
	cout<<endl<<"BIG O of Bubble Sort is SMALL: "<<bigO<<endl;
	/*bub.open ("bub.txt", std::ofstream::out);
	bub <<  "Testing\n";
	bub << "BIG-O Bubble Sort: " << bigO << "\t \n";
	bub.close()*/
	bigO = 0;

	//copied small array into cpy
	//int non = 100;
        copy(small, small+100, cpy);

	shellSort(small,100,bigO);
        cout<<"BIG O of Shell Sort is SMALL: "<<bigO<<endl;
        bigO=0;
	
	quickSort(cpy,0,99,bigO);
	cout<<endl<<"BIG O of Quick sort is SMALL: "<<bigO<<endl;
	/*quick.open ("quick.txt", std::ofstream::out);
        quick << "Testing\n";
        quick << "\n BIG-O Quick Sort: "<< bigO <<"\t \n";
        quick.close();*/
	bigO = 0;

	selectionSort(small,100,bigO);
	cout<<endl<<"BIG O of Selection sort is SMALL: "<<bigO<<endl;
	/*selection.open ("selection.txt", std::ofstream::out);
        selection << "Testing\n";
        selection << "\n BIG-O selection: " << bigO << "\t \n";
        selection.close();*/
	bigO=0;
	
	insertionSort(small,100,bigO);
	cout<<endl<<"BIG O of Insertion sort is SMALL: "<<bigO<<endl;
	/*insertion.open ("insertion.txt", std::ofstream::out);
        insertion << "Testing \n";
        insertion << "\n BIG-O insertion : " << bigO << "\t \n";
        insertion.close();*/
	bigO=0;
	
	//inserts array into binary search tree
	for (d=0;d<100;d++)
	{
		bsmall.insert(small[d].key, small[d].data);
	}
	
	//bsmall.print_inorder();

/*************medium sorts**************/
	cout << "Medium Sorts" << endl;
	
	bubbleSort(medium,500,bigO);
	cout<<endl<<"BIG O of Bubble Sort is MEDIUM: "<<bigO<<endl;
        /*bub.open ("bub.txt", std::ofstream::out);
        bub << "Testing \n" ;
        bub << "\n BIG-O Bubble: " << bigO << "\t \n";
        bub.close();*/
	bigO=0;
	
	//copied medium array into cpy
	//non=500;
	copy(medium, medium+500, cpy);

	quickSort(cpy,0,498,bigO);
	cout<<endl<<"BIG O of Quick Sort is MEDIUM: "<<bigO<<endl;
	/*quick.open ("quick.txt", std::ofstream::out);
        quick << "Testing\n";
        quick << "\n BIG-O Quick Sort: " << bigO << "\t \n";
        quick.close();*/
	bigO=0;
 	
	//shellSort(medium,500,bigO);
        //cout<<endl<<bigO<<endl;
        //bigO=0;

	selectionSort(medium,500,bigO);
	cout<<endl<<"BIG O of Selection Sort is MEDIUM: "<<bigO<<endl;
	/*selection.open ("selection.txt", std::ofstream::out);
        selection << "Testing \n";
        selection << "\n BIG-O selection: " << bigO << "\t \n";
        selection.close();*/
	bigO=0;
	
	insertionSort(medium,500,bigO);
	cout<<endl<<"BIG O of Insertion Sort is MEDIUM: "<<bigO<<endl;
	/*insertion.open ("insertion.txt", std::ofstream::out | std::ofstream::app);
        insertion << "Testing\n";
        insertion << "\n BIG-O insertion: " << bigO << "\t \n";
        insertion.close();*/
	bigO=0;
	
	//inserts array into binary search tree
	for (d=0;d<500;d++)
	{
		bmed.insert(medium[d].key, medium[d].data);
	}
	
	//bmed.print_inorder();

/*************large sorts**************/
	cout << "Large Sorts" << endl;
	bubbleSort(large,1000,bigO);
	cout<<endl<<"BIG O of Bubble Sort is LARGE: "<<bigO<<endl;	
        //bub1000.open ("bub1000.dat");
        //bub1000 << "Testing \n" << endl;
        //bub1000 << "\n BIG-O Bubble: " << bigO;
        //bub1000.close();
	bigO=0;
	
	//copied large array into cpy
	//non=1000;
	copy(large, large+1000, cpy);

	quickSort(cpy,0,999,bigO);
	cout<<endl<<"BIG O of quick Sort is LARGE: "<<bigO<<endl;
	//quick1000.open ("quick1000.dat");
        //quick1000 >> "Testing \n" >> endl;
        //quick1000 >> "\n BIG-O Quick Sort: " >> bigO;
        //quick1000.close();
	bigO=0;

	//shellSort(large,1000,bigO);
        //cout<<endl<<bigO<<endl;
        //bigO=0;
	
	selectionSort(large,1000,bigO);
	cout<<endl<<"BIG O of Selection Sort is LARGE: "<<bigO<<endl;
	//selection1000.open ("selection1000.dat");
        //selection1000 >> "Testing \n" >> endl;
        //selection1000 >> "\n BIG-O Selection: " >> bigO;
        //selection1000.close();
	bigO=0;
	
	insertionSort(large,1000,bigO);
	cout<<endl<<"BIG O of Insertion Sort is LARGE: "<<bigO<<endl;
	//insertion1000.open ("insertion1000.dat");
        //insertion1000 >> "Testing \n" >> endl;
        //insertion1000 >> "\n BIG-O insertion: " >> bigO;
        //insertion1000.close();
	bigO=0;
	
	//inserts array into binary search tree
	for (d=0;d<1000;d++)
	{
		blarge.insert(large[d].key, large[d].data);
	}
	
	//blarge.print_inorder();

	
	return 0;
	
	
}
