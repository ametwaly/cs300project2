/*************************************************************************************
Quick Sort Pseudo Code

Description of the Problem:
	This program will generate three unsorted lists of data of different sizes: 50, 5000, and 100,000 items.
	The unsorted lists will consist of a data structure that contains the key - a random integer and a random string (data)
Pseudo Code:
  quick sort
Data:
  bigO                                - iterations counter 
  key				      - randomly generated key used for sort
  data				      - randomly generated data used in sort

Parameter:
  arr[]                               - copied arry from main
  left                                - 0 of arry
  right                               - right most of arry depending on the size of the array passed in
  bigO                                - iterator counter
 ************************************************************************************/

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>

#include "list.cpp"

#ifndef __quick
#define __quick

using namespace std;



void quickSort(list arr[], int left, int right, int &bigO)
{
      int i = left, j = right;

      list tmp;
      int pivot = arr[(left + right) / 2].key;


      while (i <= j)
	  {
            while (arr[i].key < pivot)
			{
                  i++;
				  bigO++;
			}
            while (arr[j].key > pivot)
			{
                  j--;
				  bigO++;
			}
            if (i <= j)
			{
                  tmp = arr[i];
                  arr[i].key = arr[j].key;
				  arr[i].data = arr[j].data;

                  arr[j].key = tmp.key;
				  arr[j].data =tmp.data;

                  i++;
                  j--;

            }

      };


      if (left < j)
            quickSort(arr, left, j,bigO);
      if (i < right)
            quickSort(arr, i, right,bigO);
}
#endif
