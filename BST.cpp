#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <cstdlib>

#include "list.cpp"

#ifndef __bst
#define __bst

using namespace std;
// Node class
class Node {
    int key;
    Node* left;
    Node* right;
public:
    Node() { key=-1; left=NULL; right=NULL; };
    void setKey(int aKey) { key = aKey; };
    void setLeft(Node* aLeft) { left = aLeft; };
    void setRight(Node* aRight) { right = aRight; };
    int Key() { return key; };
    Node* Left() { return left; };
    Node* Right() { return right; };
};

// BinarySearchTree class
class BinarySearchTree {
     Node* root;
public:
     BinarySearchTree();
     ~BinarySearchTree();
     Node* Root() { return root; };
     void insert(int key);
     void inOrder(Node* n);
     void preOrder(Node* n);
     void postOrder(Node* n);
private:
     void insert(int key, Node* leaf);
     void freeNode(Node* leaf);
};

// Constructor
BinarySearchTree::BinarySearchTree() {
     root = NULL;
}

// Destructor
BinarySearchTree::~BinarySearchTree() {
     freeNode(root);
}

// Free the node
void BinarySearchTree::freeNode(Node* leaf)
{
    if ( leaf != NULL )
    {
       freeNode(leaf->Left());
       freeNode(leaf->Right());
       delete leaf;
    }
}

// Add a node
void BinarySearchTree::insert(list arr[].key, list arr[].data, &bigO) {
     // No elements. Add the root
     if ( root == NULL ) {
        cout << "add root node ... " << key << endl;
        Node* n = new Node();
        n->setKey(key);
        root = n;
     }
     else {
       cout << "add other node ... " << key << endl;
       insert(key, root);
     }
}

// Add a node (private)
void BinarySearchTree::insert(int key, Node* leaf) {
    if ( key <= leaf->Key() ) {
       if ( leaf->Left() != NULL )
          insert(key, leaf->Left());
       else {
          Node* n = new Node();
          n->setKey(key);
          leaf->setLeft(n);
       }
    }
    else {
       if ( leaf->Right() != NULL )
          insert(key, leaf->Right());
       else {
          Node* n = new Node();
          n->setKey(key);
          leaf->setRight(n);
       }
    }
}

// Print the BinarySearchTree in-order
// Traverse the left sub-BinarySearchTree, root, right sub-BinarySearchTree
void BinarySearchTree::inOrder(Node* n) {
    if ( n ) {
       inOrder(n->Left());
       cout << n->Key() << " ";
       inOrder(n->Right());
    }
}

// Print the BinarySearchTree pre-order
// Traverse the root, left sub-BinarySearchTree, right sub-BinarySearchTree
void BinarySearchTree::preOrder(Node* n) {
    if ( n ) {
       cout << n->Key() << " ";
       preOrder(n->Left());
       preOrder(n->Right());
    }
}

// Print the BinarySearchTree post-order
// Traverse left sub-BinarySearchTree, right sub-BinarySearchTree, root
void BinarySearchTree::postOrder(Node* n) {
    if ( n ) {
       postOrder(n->Left());
       postOrder(n->Right());
       cout << n->Key() << " ";
    }
}
#endif
