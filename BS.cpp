#include <iostream>
#include <math.h>
#include "list.cpp"

#ifndef __BST__
#define __BST__

class BinarySearchTree
{
    private:
        struct tree_node
        {
           tree_node* left;
           tree_node* right;
           int key;
		   double data;
        };
        tree_node* root;
    public:
        BinarySearchTree()
        {
           root = NULL;
        }
        bool isEmpty() const 
        { return root==NULL; }
        void print_inorder();
        void inorder(tree_node*);
        void insert( int,double);

        
        
        
}

// Smaller elements go left
// larger elements go right

void BinarySearchTree::insert(int k, double d)
{
    tree_node* t = new tree_node;
    tree_node* parent;
    t->key = k;
	t->data = d;
    t->left = NULL;
    t->right = NULL;
    parent = NULL;
  //if empty node the data will be set as the root
  if(isEmpty()) 
    root = t;
  else
  {
    //Note: ALL insertions are as leaf nodes
    tree_node* curr;
    curr = root;
    // Find the Node's parent
    while(curr)
    {
        parent = curr;
        if(t->key > curr->key) 
			curr = curr->right;
        else 
			curr = curr->left;
		
    }
    if(t->key < parent->key)
       parent->left = t;
    else
       parent->right = t;
  }
}

void BinarySearchTree::print_inorder()
{    
      
       inorder(root);
      
       
}

void BinarySearchTree::inorder(tree_node* p)
{    
    
    if(p != NULL)
    {
       
        if(p->left) inorder(p->left);
         cout <<" "<<p->key <<endl;
		 //<<" "<< p->data <<endl; 
        if(p->right) inorder(p->right);
      
    }
    
    else return;
   
}
#endif
