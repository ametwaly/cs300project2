/*************************************************************************************

Project Pseudo Code
	
Description of the Problem:
	This program will generate three unsorted lists of data of different sizes: 100, 500, and 1000 items. 
	The unsorted lists will consist of a data structure that contains the key - an integer and a random double number
Pseudo Code:
	main 
		randomly generate numbers and data
		pass key and data into each sort 
		output results into separate respective files 
		print onscreen the results for each sort
Data:
	int i = 0;													- loop counter
	int bigO = 0;												- iteration counter for each sort
	int n = 0;													- number of list
	int d = 0;													- counter for inserting into binary search tree
	int q = 0;													- counter for inserting into each sort	
	key														- randomly generated key used for sort
	data														- randomly generated data used in sort
	small														- array of list 100	 
	medium													- array of list 500 
	large 														- array of list 1000
	
Functions:
 	bubbleSort												- bubble sort called
 	insertionSort												- insertion sort called
 	selectionSort												- selection sort called
 	quickSort													- quick sort called 

Main:
	for loops for generating random keys and data
	list 100
		puts copy of key and data into each sorts
		writes sorted copy list from each sort into files
		inserts list into binary search tree
		calls print in order function from bst class to write out nodes into file
		counts
		prints out iteration calculation for each sort
		
 ************************************************************************************/	

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>

#include "bubble.cpp"
#include "selection.cpp"
#include "insertion.cpp"
#include "quick.cpp"
#include "bst.cpp"
#include "list.cpp"

using namespace std;

int main (void)
{

	ofstream quick100;
	ofstream quick500;
	ofstream quick1000;
	
	
	ofstream bsma;
	ofstream bme;
	ofstream blar;
	
	BinarySearchTree bsmall;
	BinarySearchTree bmed;
	BinarySearchTree blarge;
	
	srand((int)time(NULL));
	
	int i = 0;
	int bigO = 0;
	int n = 0;
	int d = 0;
	int q = 0;
	
	list small[100];
	list medium[500];
	list large[1000];
	list qcpy[1000];
	
	
	for (i=0;i<=99;i++)
	{
		small[i].key = (1 + rand());
		small[i].data = (double)rand(); 	
		
	}
	
	for (i=0;i<=499;i++)
	{
		medium[i].key = (1 + rand()); 
		medium[i].data = (double)rand();
	}
	
	for (i=0;i<=999;i++)
	{
		large[i].key = (1 + rand()); 
		large[i].data = (double)rand();
	}
	
/*************small sorts**************/
	cout << endl << "*************List 100*************" << endl;
	
	bubbleSort(small,100,bigO);
	bigO=0;
	
	selectionSort(small,100,bigO);
	bigO=0;
	
	insertionSort(small,100,bigO);
	bigO=0;
	
	
	//copied small array into cpy
	n=100;
	copy(small, small+n, qcpy);
	quickSort(qcpy,0,99,bigO);

    quick100.open ("quick100.dat");   
    quick100 << "list 100:" << endl << endl;
    
	for(q=0;q<100;q++)
    {
		quick100 << qcpy[q].key << "  " << qcpy[q].data << endl;
    }
    quick100.close();
	
    cout << endl << "Quick Big O calculated time: 200";
    cout << endl << "Quick sort time : " << bigO << endl;
    bigO=0;
	
		
	//inserts array into binary search tree
	for (d=0;d<100;d++)
	{
		bsmall.insert(small[d].key, small[d].data, bigO);
	}
	
	cout << endl << "Binary Search Tree Big 0 calculated time: 10000";
	cout<< endl << "Binary Search Tree Sort: " << bigO << endl;
	
	bsma.open ("BSTSmall.dat");
	
	bsmall.print_inorder(bsma);
	
	bsma.close();
	
/*************medium sorts**************/
	cout << endl << "*************List 500*************" << endl;
	bubbleSort(medium,500,bigO);

	bigO=0;
	selectionSort(medium,500,bigO);

	bigO=0;
	insertionSort(medium,500,bigO);

	bigO=0;	
	//copied medium array into cpy
	n=500;
	copy(medium, medium+n, qcpy);
	quickSort(qcpy,0,499,bigO);
	
	quick500.open ("quick500.dat");   
    quick500 << "list 500:" << endl << endl;
    
	for(q=0;q<500;q++)
    {
		quick500 << qcpy[q].key << "  " << qcpy[q].data << endl;
    }
    quick500.close();
	
    cout << endl << "Quick Big O calculated time: 1349.5";
	cout << endl << "Quick sort time : " << bigO << endl;
    bigO=0;

	//inserts array into binary search tree
	for (d=0;d<500;d++)
	{
		bmed.insert(medium[d].key, medium[d].data, bigO);
	}
	
	cout << endl << "Binary Search Tree Big 0 calculated time: 250000";
	cout<< endl << "Binary Search Tree Sort: " << bigO << endl;
	
	
	//print medium bst
	bme.open ("BSTMedium.dat");
	bmed.print_inorder(bme);
	
	bme.close();
	
/*************large sorts**************/
	cout << endl << "*************List 1000*************" << endl;
	
	bubbleSort(large,1000,bigO);

	bigO=0;
	selectionSort(large,1000,bigO);

	bigO=0;
	insertionSort(large,1000,bigO);

	bigO=0;
   //copied large array into cpy
	n=1000;
	copy(large, large+n, qcpy);
	quickSort(qcpy,0,999,bigO);
    
	quick100.open ("quick1000.dat");   
    quick1000 << "list 1000:" << endl << endl;
    
	for(q=0;q<1000;q++)
    {
		quick1000 << qcpy[q].key << "  " << qcpy[q].data << endl;
    }
    quick1000.close();
	
    cout << endl << "Quick Big O calculated time: 3000"; 
	cout << endl << "Quick sort time : " << bigO << endl;
    bigO=0;

	//inserts array into binary search tree
	for (d=0;d<1000;d++)
	{
		blarge.insert(large[d].key, large[d].data, bigO);
	}
	
	cout << endl << "Binary Search Tree Big 0 calculated time: 1000000";
	cout<< endl << "Binary Search Tree Sort: " << bigO << endl << endl;
	
	//print large bst to file
	blar.open ("BSTLarge.dat");
	blarge.print_inorder(blar);
	
	blar.close();

	
	return 0;
	
	
}
