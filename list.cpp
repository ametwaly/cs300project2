/*************************************************************************************
List Pseudo Code

Description of the Problem:
	This program will generate three unsorted lists of data of different sizes: 50, 5000, and 100,000 items.
	The unsorted lists will consist of a data structure that contains the key - a random integer and a random string data
Pseudo Code:
  list structure
Data:

	key				- randomly generated key used for sort
	data			        - randomly generated data used in sort (string)

 ************************************************************************************/
#include <string>

#ifndef __list
#define __list

using namespace std;

struct list
{
	int key;
	std::string data;
};

#endif
