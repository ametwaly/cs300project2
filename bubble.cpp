/*************************************************************************************
Bubble Pseudo Code

Description of the Problem:
	This program will generate three unsorted lists of data of different sizes: 50, 5000, and 100,000 items.
	The unsorted lists will consist of a data structure that contains the key - a random integer and a random string 
Pseudo Code:
  bubble sort algorithm
Data:
	int bigO = 0;												- iteration counter for each sort
	key											                - randomly generated key used for sort
	data											                - randomly generated data used in sort
	small												        - array of list 50
	medium													- array of list 5000
	large 												        - array of list 100,000

Functions:
 	bubbleSort												  - bubble sort called

Parameter:
  arr[]                               - copied arry from main
  n                                   - number of amount of array
  bigO                                - iterator counter
 ************************************************************************************/

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>

#include "list.cpp"

#ifndef __bubble
#define __bubble

using namespace std;

void bubbleSort(list arr[], int n, int &bigO)
{
   ofstream bub50;
   ofstream bub5000;
   ofstream bub100000;


      bool swapped = true;
      int j = 0;
      int x = 0;
      list tmp;
      list cpy[n];
      copy(arr, arr+n, cpy);
	  while (swapped)
	  {
            swapped = false;
            j++;
            for (int i = 0; i < n - j; i++)
			{
                  if (cpy[i].key > cpy[i + 1].key)
				  {
                        tmp = cpy[i];
                        cpy[i].key = cpy[i + 1].key;
						cpy[i].data=cpy[i+1].data;

                        cpy[i + 1].key = tmp.key;
						cpy[i+1].data= tmp.data;

                        swapped = true;
                  }
			 bigO++;
            }
      }

      if(n==50)
      {
        bub50.open ("bub50.txt");

        bub50 << "list 50:" << endl << endl;
        for(x=0;x<50;x++)
        {
           bub50<< cpy[x].key << "  " << cpy[x].data << endl;
        }

	cout << endl << "Bubble Sort Big-O calculated value: O(n^2) = 2500" << endl << "Bubble Sort operations: " << bigO << endl;

        bub50.close();
      }

      if(n==5000 && repassing == false)
      {
        bub5000.open ("bub5000.txt");

        bub5000 << "list 5000:" << endl << endl;

        for(x=0;x<5000;x++)
        {
           bub5000<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Bubble Sort Big-O calculated value: O(n^2) = 2500" << endl << "Bubble Sort operations: " << bigO << endl;

        bub5000.close();
      }
      if(n==100000)
      {
        bub100000.open ("bub100000.txt");

        bub100000 << "list 100000:" << endl << endl;

        for(x=0;x<100000;x++)
        {
           bub100000<< cpy[x].key << "  " << cpy[x].data << endl;
        }

        cout << endl << "Bubble Sort Big-O calculated value: O(n^2) = 2500" << endl << "Bubble Sort operations: " << bigO << endl;

        bub100000.close();
      }

};

 #endif
