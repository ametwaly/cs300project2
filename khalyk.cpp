/*************************************************************************************
Project Pseudo Code

Description of the Problem:
	This program will generate three unsorted lists of data of different sizes: 100, 500, and 1000 items.
	The unsorted lists will consist of a data structure that contains the key - an integer and a random double number
Pseudo Code:
	main
		randomly generate numbers and data
		pass key and data into each sort
		output results into separate respective files
		print onscreen the results for each sort
Data:
	int i = 0;													- loop counter
	int bigO = 0;												- iteration counter for each sort
	int n = 0;													- number of list
	int d = 0;													- counter for inserting into binary search tree
	int q = 0;													- counter for inserting into each sort
	key														- randomly generated key used for sort
	data														- randomly generated data used in sort
	small														- array of list 100
	medium													- array of list 500
	large 														- array of list 1000

Functions:
 	bubbleSort												- bubble sort called
 	insertionSort												- insertion sort called
 	selectionSort												- selection sort called
 	quickSort													- quick sort called

Main:
	for loops for generating random keys and data
	list 100
		puts copy of key and data into each sorts
		writes sorted copy list from each sort into files
		inserts list into binary search tree
		calls print in order function from bst class to write out nodes into file
		counts
		prints out iteration calculation for each sort

 ************************************************************************************/
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>

#include "bubble.cpp"
#include "selection.cpp"
#include "insertion.cpp"
#include "quick.cpp"
#include "bst.cpp"
#include "list.cpp"

using namespace std;

int main (void)
{

	ofstream quick50;
	ofstream quick5000;
	ofstream quick10000;


	//ofstream bsma;
	//ofstream bme;
	//ofstream blar;

	//BinarySearchTree bsmall;
	//BinarySearchTree bmed;
	//BinarySearchTree blarge;

	srand((int)time(NULL));

	int i = 0;
	int bigO = 0;
	int n = 0;
	//int d = 0;
	int q = 0;

	list small[50];
	list medium[5000];
	list large[10000];
	list qcpy[n];


	for (i=0;i<=49;i++)
	{
		small[i].key = (1 + rand());
		small[i].data = (double)rand();

	}

	for (i=0;i<=4999;i++)
	{
		medium[i].key = (1 + rand());
		medium[i].data = (double)rand();
	}

	for (i=0;i<=9999;i++)
	{
		large[i].key = (1 + rand());
		large[i].data = (double)rand();
	}

/*************small sorts**************/
	cout << endl << "*************List 50*************" << endl;

	bubbleSort(small,50,bigO);
	bigO=0;

	selectionSort(small,50,bigO);
	bigO=0;

	insertionSort(small,50,bigO);
	bigO=0;


	//copied small array into cpy
	n=50;
	copy(small, small+n, qcpy);
	quickSort(qcpy,0,49,bigO);

    quick50.open ("quick50.txt");
    quick50 << "list 50:" << endl << endl;

	for(q=0;q<50;q++)
    {
		quick50 << qcpy[q].key << "  " << qcpy[q].data << endl;
    }
    quick50.close();

    cout << endl << "Quick Big O calculated time: 200";
    cout << endl << "Quick sort time : " << bigO << endl;
    bigO=0;


	/*inserts array into binary search tree
	for (d=0;d<100;d++)
	{
		bsmall.insert(small[d].key, small[d].data, bigO);
	}

	cout << endl << "Binary Search Tree Big 0 calculated time: 10000";
	cout<< endl << "Binary Search Tree Sort: " << bigO << endl;

	bsma.open ("BSTSmall.dat");

	bsmall.print_inorder(bsma);

	bsma.close();*/

/*************medium sorts**************/
	cout << endl << "*************List 5000*************" << endl;
	bubbleSort(medium,5000,bigO);

	bigO=0;
	selectionSort(medium,5000,bigO);

	bigO=0;
	insertionSort(medium,5000,bigO);

	bigO=0;
	//copied medium array into cpy
	n=5000;
	copy(medium, medium+n, qcpy);
	quickSort(qcpy,0,4999,bigO);

	quick5000.open ("quick500.txt");
    quick5000 << "list 500:" << endl << endl;

	for(q=0;q<5000;q++)
    {
		quick5000 << qcpy[q].key << "  " << qcpy[q].data << endl;
    }
    quick5000.close();

    cout << endl << "Quick Big O calculated time: 1349.5";
	cout << endl << "Quick sort time : " << bigO << endl;
    bigO=0;

	/*inserts array into binary search tree
	for (d=0;d<500;d++)
	{
		bmed.insert(medium[d].key, medium[d].data, bigO);
	}

	cout << endl << "Binary Search Tree Big 0 calculated time: 250000";
	cout<< endl << "Binary Search Tree Sort: " << bigO << endl;


	//print medium bst
	bme.open ("BSTMedium.dat");
	bmed.print_inorder(bme);

	bme.close();*/

/*************large sorts**************/
	cout << endl << "*************List 100000*************" << endl;

	bubbleSort(large,10000,bigO);

	bigO=0;
	selectionSort(large,10000,bigO);

	bigO=0;
	insertionSort(large,10000,bigO);

	bigO=0;
   //copied large array into cpy
	n=10000;
	copy(large, large+n, qcpy);
	quickSort(qcpy,0,9999,bigO);

	quick10000.open ("quick10000.txt");
    quick10000 << "list 10000:" << endl << endl;

	for(q=0;q<10000;q++)
    {
		quick10000 << qcpy[q].key << "  " << qcpy[q].data << endl;
    }
    quick10000.close();

    cout << endl << "Quick Big O calculated time: 3000";
	cout << endl << "Quick sort time : " << bigO << endl;
    bigO=0;

	/*inserts array into binary search tree
	for (d=0;d<1000;d++)
	{
		blarge.insert(large[d].key, large[d].data, bigO);
	}

	cout << endl << "Binary Search Tree Big 0 calculated time: 1000000";
	cout<< endl << "Binary Search Tree Sort: " << bigO << endl << endl;

	//print large bst to file
	blar.open ("BSTLarge.dat");
	blarge.print_inorder(blar);

	blar.close();*/


	return 0;

}
