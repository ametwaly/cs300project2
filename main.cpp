/*************************************************************************************
	Name: Amr Metwaly 
	WSU ID: u377u468

	Name: Levi Lowe
	WSU ID: j298t623

Project Pseudo Code

Description of the Problem:
	This program will generate three unsorted lists of data of different sizes: 50, 5000, and 100,000 items.
	The unsorted lists will consist of a data structure that contains the key - an integer and a string random variables 
Pseudo Code:
	main
		randomly generate numbers and data
		pass key and data into each sort
		output results into separate respective files
		print onscreen the results for each sort. Use
		the same data for BST in-order traversal(WIP). 
Data:
	int i = 0;													- loop counter
	int bigO = 0;												        - iteration counter for each sort
	int n = 0;													- number of list
	int d = 0;													- counter for inserting into binary search tree
	int q = 0;													- counter for inserting into each sort
	key														- randomly generated key used for sort
	data														- randomly generated data used in sort
	small														- array of list 50
	medium													        - array of list 5000
	large 														- array of list 100000

Functions:
 	bubbleSort												- bubble sort called
 	insertionSort												- insertion sort called
 	selectionSort												- selection sort called
 	quickSort												- quick sort called
        shellSort                                                                                               - shell sort called
	genRandom                                                                                               - generate random strings 

Main:
	for loops for generating random keys and strings (data)
	list 100
		puts copy of key and data into each sorts
		writes sorted copy list from each sort into files
		inserts list into binary search tree
		calls print in order function from bst class to write out nodes into file
		counts
		prints out iteration calculation for each sort

 ************************************************************************************/
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cstring>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include "shellSort.cpp"
#include "bubble.cpp"
#include "selection.cpp"
#include "insertion.cpp"
#include "quick.cpp"
#include "bst.cpp"
#include "list.cpp"

using namespace std;


char genRandom();  // Random string generator function.

int main (void)
{	
	ofstream quick50;
	ofstream quick5000;
	ofstream quick100000;


	ofstream bsma;
	ofstream bme;
	ofstream blar;

	BinarySearchTree bsmall;
	BinarySearchTree bmed;
	BinarySearchTree blarge;
	BinarySearchTree bmedRe;

	
	srand((int)time(NULL));

	//Each sort checks for the value of repassing to determine the proper output file
	//TODO: modify each sort to look for repassing
        // bool repassing = false;
	
	int i = 0;
	int bigO = 0;
	int n = 0;
	int d = 0;
	int q = 0;

	list small[50];
	list medium[5000];
	list large[100000];
	list qcpy[100000];

	for (i=0;i<=49;i++)
	{
		small[i].key = (1 + rand());

		//generate a random string of length 22
		for(unsigned int d=0; d < 21; d++)
                {
                    small[i].data += genRandom();
                }

	}

	for (i=0;i<=4999;i++)
	{
		medium[i].key = (1 + rand());

		//generate a random string of length 22
		for(unsigned int d=0; d < 21; d++)
                {
                    medium[i].data += genRandom();
		}
	}

	for (i=0;i<=99999;i++)
	{
		large[i].key = (1 + rand());

		//generate a random string of length 22
		for(unsigned int d=0; d < 21; d++)
                {
                    large[i].data += genRandom();
                }
	}

/*************SMALL SORTS**************/
	cout << endl << "*************List 50*************" << endl;
        shellSort(small,50,bigO);
        bigO=0;

	bubbleSort(small,50,bigO);
	bigO=0;

	selectionSort(small,50,bigO);
	bigO=0;

	insertionSort(small,50,bigO);
	bigO=0;


	//copied small array into cpy
	n=50;
	copy(small, small+n, qcpy);
	quickSort(qcpy,0,49,bigO);

        quick50.open ("quick50.txt");
        quick50 << "list 50:" << endl << endl;

	for(q=0;q<50;q++)
        {
	   quick50 << qcpy[q].key << "  " << qcpy[q].data << endl;
        }
        quick50.close();

        cout << endl << "Quick Sort Big-O calculated value: O(n^2) = 2500";
        cout << endl << "Quick Sort operations: " << bigO << endl;
        bigO=0;

	bsma.open ("bst50.txt");
	bsma << "list 50: \n" << endl;

	//inserts array into binary search tree
	for (d=0;d<50;d++)
	{
		bsmall.insert(small[d].key, small[d].data, bigO);
		bsma << small[d].key << "  " << small[d].data << endl;
	}
	bsma.close();

	cout << endl << "Binary Search Tree Sort Big-0 calculated value: 50";
	cout<< endl << "Binary Search Tree Sort operations: " << bigO << endl;

/*************MEDIUM SORTS**************/
	cout << endl << "*************List 5000*************" << endl;
        shellSort(medium,5000,bigO);
        bigO=0;

	bubbleSort(medium,5000,bigO);
	bigO=0;

	selectionSort(medium,5000,bigO);
	bigO=0;

	insertionSort(medium,5000,bigO);
	bigO=0;

	//copied medium array into cpy
	n=5000;
	copy(medium, medium+n, qcpy);
	quickSort(qcpy,0,4999,bigO);

	quick5000.open ("quick5000.txt");
        quick5000 << "list 5000:" << endl << endl;
	for(q=0;q<5000;q++)
        {
	  quick5000 << qcpy[q].key << "  " << qcpy[q].data << endl;
        }
        quick5000.close();

        cout << endl << "Quick Big-O calculated value: O(n^2) = 25000000";
	cout << endl << "Quick Sort operations : " << bigO << endl;
        bigO=0;
	
	bme.open ("bst5000.txt");
        bme << "list 5000: \n" << endl;

	//inserts array into binary search tree
	for (d=0;d<5000;d++)
	{
		bmed.insert(medium[d].key, medium[d].data, bigO);
		bme << medium[d].key << "  " << medium[d].data << endl;
	}
	bme.close();

	cout << "\nBinary Search Tree Sort Big-0 calculated value: O(n) = 5000";
	cout<< endl << "Binary Search Tree Sort operations: " << bigO << endl << endl;

/*************LARGE SORTS**************/
	cout << endl << "*************List 100,000*************" << endl;
	shellSort(large,100000,bigO);
	bigO=0;
        
	bubbleSort(large,100000,bigO);

	bigO=0;
	selectionSort(large,100000,bigO);

	bigO=0;
	insertionSort(large,100000,bigO);

	bigO=0;
        //copied large array into cpy
	n=100000;
	copy(large, large+n, qcpy);
	quickSort(qcpy,0,99999,bigO);

	quick100000.open ("quick100000.txt");
        quick100000 << "list 100000:" << endl << endl;
	for(q=0;q<100000;q++)
        {
           quick100000 << qcpy[q].key << "  " << qcpy[q].data << endl;
        }
        quick100000.close();

        cout << endl << "Quick Sort Big-O calculated value: O(n^2) = 10000000000";
	cout << endl << "Quick Sort operations: " << bigO << endl;
        bigO=0;
	
	blar.open ("bst100000.txt");
        blar << "list 100,000: \n" << endl;

	//inserts array into binary search tree
	for (d=0;d<100000;d++)
	{
		blarge.insert(large[d].key, large[d].data, bigO);
		blar << large[d].key << "  " << large[d].data << endl;
	}
	blar.close();

	cout << endl << "Binary Search Tree Sort Big-0 calculated value: O(n) = 100,000";
	cout<< endl << "Binary Search Tree Sort operations: " << bigO << endl << endl;

/************************passing in the 5000 data sorted set****/
        cout << "/********** passing in the 5000 data sorted set ***********/" << endl;

	// Declare the file stream
        ifstream unsorted;
        ofstream sorted;
	repassing = true;

        //to reduce the code
        string sorts[] = {"quick", "sins", "bub", "sels", "shell", "bst"};
        string calls[] = {"quickSort", "insertionSort", "bubbleSort", "selectionSort", "shellSort", "Binary Search Tree"};
	sorted.open("sorted.txt");

        for (unsigned int i = 0; i < 6; i++)
        {
        list sortedMed[5000];
        string line, b;
        int a;
        string file = sorts[i] + "5000.txt";
        unsorted.open(file.c_str());
	//cout << "unsorted file " << file << " Is good value is : " <<  unsorted.good() << endl;
        
	if ( sorted.good()){
        int counter = 0;
           while ( counter < 5000)
           {
               getline(unsorted, line);
               stringstream ss(line);
               if (ss >> a >> b)
               {

                  // Add a, b to their respective arrays
                  sortedMed[counter].key = a;
                  sortedMed[counter].data = b;
               }
               counter++;
           }
	   
	   //Calling the sort functions
	   switch (i){
	          case 0:
			bigO=0; 
		   	//copied medium array into cpy
                        n=5000;
                        copy(sortedMed, sortedMed+n, qcpy);
                        quickSort(qcpy,0,4999,bigO);
			cout << endl << calls[i] << " Big-O calculated value: O(n^2) = 25000000";
			cout << "\nThe actual number of opertions was: " << bigO << endl << endl;	
			sorted << calls[i] << " had "<< bigO << " Operations.\n \n";
			bigO=0;
			continue;

                  case 1:
			insertionSort(sortedMed,5000,bigO);
			cout << calls[i] << " Big-O calculated value: O(n^2) = 25000000";
                        cout << "\nThe actual number of opertions was: " << bigO << endl << endl;
			sorted << calls[i] << " had "<< bigO << " Operations.\n \n";
			bigO=0;
			continue;

		  case 2:
                  	bubbleSort(sortedMed,5000,bigO);
			cout << calls[i] << " Big-O calculated value: O(n^2) = 25000000";
                        cout << "\nThe actual number of opertions was: " << bigO << endl << endl;
 			sorted << calls[i] << " had "<< bigO << " Operations.\n \n";
			bigO=0;
			continue;

		  case 3:
			selectionSort(sortedMed,5000,bigO);
                        cout << calls[i] << " Big-O calculated value: O(n^2) = 25000000";
                        cout << "\nThe actual number of opertions was: " << bigO << endl << endl;
			sorted << calls[i] << " had "<< bigO << " Operations.\n \n";
			bigO=0;
			continue;

	          case 4:
			shellSort(sortedMed,5000,bigO);
                        cout << calls[i] << " Big-O calculated value: O(n(log(n))^2) = 68411.89546";
                        cout << "\nThe actual number of opertions was: " << bigO << endl << endl;
			sorted << calls[i] << " had "<< bigO << " Operations.\n \n";
			bigO=0;
			continue;

		  case 5:

                  	//inserts array into binary search tree
                  	for (int d=0;d<5000;d++)
                  	{
                     		bmedRe.insert(sortedMed[d].key, sortedMed[d].data, bigO);
                  	}
                        cout << calls[i] << " Big-O calculated value: O(n^2) = 5000";
                        cout << "\nThe actual number of opertions was: " << bigO << endl << endl;
			sorted << calls[i] << " had "<< bigO << " Operations.\n \n";
			bigO=0;
			continue;
		  default:
			continue;
	
                  }//closing switch statement 
	}//closing the if ...good() statement
	unsorted.close(); 

	}//closing the for loop 

	sorted.close();

/****************************************************************/

/************************paragraph section***********/

        ofstream analysis;
	analysis.open("analyze.txt");
	analysis << "Questions answered in-order: \n1. The results do agree with the theoritical"
		    "calculations and there is no controversy in the results.\n"
		    "2. BST sorting is the fastest then followed by the ShellSort. \n"
		    "3. Data size makes a huge difference when it came to 100,000 dataset, shell"
		    "Sort and BST were significantly faster than all the other sorts because of their"
		    "BIG-O logarithmic and linear time complexities."; 
	analysis.close();

/************************end of paragraph section****/

	return 0;

}

//Function to generate random string
        static const char alphanum[] =
        "0123456789"
        "!@#$%^&*"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

int stringLength = sizeof(alphanum) - 1;

char genRandom()  // Random string generator function.
{   
    return alphanum[rand() % stringLength];
}

