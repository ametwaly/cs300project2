#include <string>
#include <iostream>

#ifndef ___bst___
#define ___bst___

using namespace std;

class BinarySearchTree
{
    private:
        struct tree_node
        {
           tree_node* left;
           tree_node* right;
           int key;
                   double data;
        };
        tree_node* root;
    public:
        BinarySearchTree()
        {
           root = NULL;
        }
        bool isEmpty() const
        { return root==NULL; }
        void print_inorder();
        void inorder(tree_node*);
        void insert( int,double);
}
#endif 
